const defer = require('config/defer').deferConfig
const path = require('path')

module.exports = {
  api: {
    host: '127.0.0.1',
    port: '3000',
    url: defer(cfg => 'http://' + cfg.api.host + ':' + cfg.api.port)
  }
}
