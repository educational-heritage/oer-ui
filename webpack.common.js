const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {

  entry: {
    app: './src/app.js',
  },

  resolve: {
    extensions: [
      '.js',
      '.scss',
      '.css',
      '.jpg',
      '.jpeg',
      '.png',
      '.svg',
    ],
    alias: {
      stylesheets: path.resolve(__dirname, './src/assets/stylesheets'),
      fonts: path.resolve(__dirname, './src/assets/fonts'),
      icons: path.resolve(__dirname, './src/assets/icons'),
      images: path.resolve(__dirname, './src/assets/images'),
      controllers: path.resolve(__dirname, './src/controllers'),
      utils: path.resolve(__dirname, './src/utils/'),
      providers: path.resolve(__dirname, './src/providers/'),
      services: path.resolve(__dirname, './src/services/'),
    },
  },

  module: {
    rules: [{
      test: /\.js$/,
      enforce: 'pre',
      exclude: /node_modules/,
      loader: 'eslint-loader',
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    }, {
      test: /\.(png|svg|jpg|jpeg|gif)$/,
      use: {
        loader: 'file-loader',
      },
    }, {
      test: /\.html$/,
      use: {
        loader: 'html-loader',
      },
    }, {
      test: /\.(eot|svg|ttf|woff|woff2)$/,
      loader: 'file-loader?name=[name].[ext]',
    }],
  },

  plugins: [
    new CleanWebpackPlugin(['dist']),
  ],

  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },

};
