FROM node:8.5.0

#RUN groupadd -r oer
#RUN adduser --system --disabled-password --gecos '' oer

WORKDIR /usr/src/app
#RUN chown -R oer /usr/src/app
#USER oer

COPY package.json .
COPY package-lock.json .

ENV PATH /usr/src/app/node_modules/.bin:$PATH

RUN npm install --ignore-scripts --unsafe-perm && \
    npm rebuild node-sass

COPY . /usr/src/app
