const webpack = require('webpack');
const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');

const common = require('./webpack.common');

const config = {
  module: {
    rules: [{
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            autoprefixer: false,
            sourceMap: false,
            importLoaders: 1,
          },
        }, {
          loader: 'sass-loader',
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: () => [autoprefixer],
          },
        }],
      }),
    }],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    new Dotenv({
      path: `./env/${process.env.NODE_ENV}`,
    }),
    new HtmlWebpackPlugin({
      template: './src/views/layout.pug',
      filename: 'layout.pug',
      chunks: ['app'],
    }),
    new HtmlWebpackPugPlugin(),
    new CopyWebpackPlugin([{
      from: './src/views',
    }], {
      ignore: [
        'layout.pug',
      ],
    }),
    new ExtractTextPlugin('style.css'),
    new UglifyJSPlugin(),
  ],
};

module.exports = merge(common, config);
