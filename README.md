# Educational Heritage - Open Educational Resources UI

[![build status](https://gitlab.com/educational-heritage/oer-api/badges/master/build.svg)](https://gitlab.com/educational-heritage/oer-ui/commits/master)

## Initial settings

- Run `npm install` to install all the dependencies.

From this point, we have the Ui development environment ready. The next step are for web development server.

- Create a file called db.json and put this code inside. 

```
{
    "resources": [
     {
        "id": 1,
        "author": "author",
        "title": "Matemáticas primero Segunda edición",
        "slug": "matematicas-primero",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1318781876406,
        "comments": 33
      }, 
      {
        "id": 2,
        "author": "author",
        "title": "Matemáticas segundo",
        "slug": "matematicas-segundo",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1504993526079,
        "comments": 2
      }, 
      {
        "id": 3,
        "author": "author",
        "title": "Matemáticas tercero",
        "slug": "matematicas-tercero",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1318781876406,
        "comments": 2
      }, 
      {
        "id": 4,
        "author": "author",
        "title": "Matemáticas cuarto",
        "slug": "matematicas-cuarto",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1318781876406,
        "comments": 127
      }, 
      {
        "id": 5,
        "author": "author",
        "title": "Matemáticas quinto",
        "slug": "matematicas-quinto",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1318781876406,
        "comments": 18
      }, 
      {
        "id": 6,
        "author": "author",
        "title": "Matemáticas sexto",
        "slug": "matematicas-sexto",
        "media": "../../assets/images/dog.png",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "detailUrl": "/entry.html",
        "publishedDate": 1504993526079,
        "comments": 0
      }
    ]
}
```

## Ui development server

Just to work on the ui, with mocked data (./mocks).

Run `npm start` for a dev server. The browser will open with the url 127.0.0.1:8080. The app will automatically reload if you change any of the source files.

## Web development server

To work in web server, with Json server as database.

- Run `npm build` to create the static files (./dist).
- Run `json-server --watch db.json --port 3004`
- Run `run start-prod` to start the production server.

The app will be avaiable on http://127.0.0.1:3000.

## Running unit tests

Run `npm test` to execute the unit tests.
