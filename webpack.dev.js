const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

const common = require('./webpack.common');

const resources = require('./mocks/resources');

const config = {
  devtool: 'cheap-module-eval-source-map',

  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    hot: true,
    inline: true,
    overlay: true,
    host: '0.0.0.0',
    port: 8080,
  },

  module: {
    rules: [{
      test: /\.scss$/,
      use: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader',
        options: {
          autoprefixer: false,
          sourceMap: false,
          importLoaders: 1,
        },
      }, {
        loader: 'sass-loader',
      }, {
        loader: 'postcss-loader',
        options: {
          plugins: () => [autoprefixer],
        },
      }],
    }, {
      test: /index.pug$/,
      use: [{
        loader: 'html-loader',
      }, {
        loader: 'pug-html-loader',
        options: {
          data: {
            resources,
            title: 'todos los recursos',
            unauthorized: false,
            url: '/',
          },
        },
      }],
    }, {
      test: /detail.pug$/,
      use: [{
        loader: 'html-loader',
        options: {
          attrs: ['img:src', 'source:srcset'],
        },
      }, {
        loader: 'pug-html-loader',
        options: {
          data: {
            resource: resources[0],
            title: resources[0].title,
            url: 'author/slug-title',
            user: {
              email: 'usuario@oer.com',
            },
          },
        },
      }],
    }, {
      test: /new-resource.pug$/,
      use: [{
        loader: 'html-loader',
      }, {
        loader: 'pug-html-loader',
        options: {
          data: {
            title: 'nuevo recurso',
            url: 'new-resource',
            user: {
              email: 'usuario@oer.com',
            },
          },
        },
      }],
    }],
  },

  plugins: [
    new Dotenv({
      path: './env/development',
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/views/index.pug',
      chunks: ['app'],
    }),
    new HtmlWebpackPlugin({
      filename: 'detail.html',
      template: './src/views/detail.pug',
      chunks: ['app'],
    }),
    new HtmlWebpackPlugin({
      filename: 'new-resource.html',
      template: './src/views/new-resource.pug',
      chunks: ['app'],
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],

};

module.exports = merge(common, config);
