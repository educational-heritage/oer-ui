const debug = require('debug')('oer_ui:router')  // eslint-disable-line
const config = require('config');
const express = require('express');

const router = express.Router();

const request = require('request');

const base = config.get('api.url');
const attachments = config.get('api.attachments');

/* GET all resources. */
router.get('/', (req, res, next) => {
  const user = { token: req.cookies.token, email: req.cookies.email };
  const unauthorized = !!req.query.unauthorized;

  const url = `${base}/persistence/resources?sort=-publishedDate`;
  request(url, (error, response, body) => {
    if (error) {
      return res.render('error/error', { error });
    }
    const { result: resources } = JSON.parse(response.body);
    resources.forEach((item, index) => { item.thumbnail = `${attachments}/${item.media}`; });

    return res.render('index', {
      resources,
      user,
      title: 'Todos los recursos',
      url: '/',
      unauthorized,
    });
  });
});

/* GET favourite resources */
router.get('/bookmarks', (req, res, next) => {
  const user = { token: req.cookies.token, email: req.cookies.email };
  const { referer } = req.headers;
  const redirectUrl = (referer && referer.split('?')[0]) || '/';
  
  if (!user.token) {
    return res.redirect(`${redirectUrl}?unauthorized=true`);
  }
  
  const { unauthorized } = res;
  
  const options = {
    url: `${base}/relationship/favourite/${user.email}/resources`,
    headers: {
      Authorization: `Bearer ${user.token}`,
    },
  };

  request(options, (error, response, body) => {
    if (error) {
      return res.render('error/error', { error });
    }

    if (response.statusCode === 401) {
      return res.redirect(`${redirectUrl}?unauthorized=true`);
    }

    if (response.statusCode === 404) {
      return res.render('no-favourites', {
        user,
        title: 'Mis recursos favoritos',
        url: '/bookmarks',
        unauthorized,
      });
    }

    const { details: resources } = JSON.parse(response.body);
    resources.forEach((item, index) => { item.favourite = true; });
    resources.forEach((item, index) => { item.thumbnail = `${attachments}/${item.media}`; });

    return res.render('index', {
      resources,
      user,
      title: 'Mis recursos favoritos',
      url: '/bookmarks',
      unauthorized,
    });
  });
});

/* GET add resource form */
router.get('/resources/new', (req, res, next) => {
  const user = { token: req.cookies.token, email: req.cookies.email };
  const { referer } = req.headers;
  const redirectUrl = (referer && referer.split('?')[0]) || '/';

  if (!user.token) {
    return res.redirect(`${redirectUrl}?unauthorized=true`);
  }

  const { unauthorized } = res;
  return res.render('new-resource', {
    title: 'Crear recurso',
    url: '/resources/new',
    user,
    unauthorized,
  });
});

/* GET one resource. */
router.get('/resources/:resourceId', (req, res, next) => {
  const resourceId = req.params.resourceId;
  const user = { token: req.cookies.token, email: req.cookies.email };
  const { referer } = req.headers;
  const redirectUrl = (referer && referer.split('?')[0]) || '/';

  if (!user.token) {
    return res.redirect(`${redirectUrl}?unauthorized=true`);
  }

  const { unauthorized } = res;
  const url = `${base}/persistence/resources/${resourceId}`;
  request(url, (error, response, body) => {
    if (error) {
      return res.render('error/error', { error });
    }
    const resource = JSON.parse(response.body);
    return res.render('detail', { resource, user, unauthorized });
  });
});

router.get('/:authorSlug/:titleSlug', (req, res, next) => {
  const user = { token: req.cookies.token, email: req.cookies.email };
  const { referer } = req.headers;
  const redirectUrl = (referer && referer.split('?')[0]) || '/';

  if (!user.token) {
    return res.redirect(`${redirectUrl}?unauthorized=true`);
  }

  const { unauthorized } = res;
  const url = `${base}/search/resources`;
  request.post(url, {
    form: {
      authorSlug: `${req.params.authorSlug}`,
      titleSlug: `${req.params.titleSlug}`,
    },
  }, (error, response, body) => {
    if (error) {
      return res.render('error/error', { error });
    }
    const resource = JSON.parse(response.body).details[0];
    resource.media = `${attachments}/${resource.media}`;
    return res.render('detail', { resource, user, unauthorized });
  });
});

module.exports = router;
