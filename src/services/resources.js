import { POST } from 'providers/fetch';
import storage from 'providers/storage';

const baseUrl = '/persistence/resources';

const addResource = async (resource) => {
  resource.append('author', storage.get('author'));
  const url = baseUrl;
  const { result, ...rest } = await POST(url, resource);
  return {
    success: result && result._id,
    ...rest,
  };
};

export default {
  addResource,
};
