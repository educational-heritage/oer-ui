import { GET, POST, DELETE } from 'providers/fetch';
import storage from 'providers/storage';

const baseUrl = '/relationship';

const getFavouriteResources = async ({ id: value }) => {
  const email = storage.get('email');
  const url = `${baseUrl}/favourite/${email}/resources`;
  const { result, ...rest } = await GET(url);
  return {
    success: result,
    ...rest,
  };
};

const addResourceToFavourites = async ({ id: value }) => {
  const email = storage.get('email');
  const url = baseUrl;
  const data = new FormData();
  data.append('foreign', 'resources');
  data.append('key', email);
  data.append('route', 'favourite');
  data.append('value', value);
  const { result, ...rest } = await POST(url, data);
  return {
    success: Array.isArray(result) && result.length,
    ...rest,
  };
};

const removeResourceFromFavourites = async ({ id: value }) => {
  const email = storage.get('email');
  const url = `${baseUrl}/favourite/${email}/resources/${value}`;
  // TODO: review that response
  const { ok, ...rest } = await DELETE(url);
  return {
    success: ok,
    ...rest,
  };
};

export {
  getFavouriteResources,
  addResourceToFavourites,
  removeResourceFromFavourites,
};
