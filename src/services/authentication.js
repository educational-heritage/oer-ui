import { POST } from 'providers/fetch';
import storage from 'providers/storage';

const baseUrl = '/authentication';

const authentication = async (url, user) => {
  const { result, ...rest } = await POST(url, user);

  if (result && result.token) {
    storage.set('token', result.token);
    storage.set('email', user.get('email'));
    storage.set('author', user.get('fullname'));
  }
  return {
    success: result && result.token,
    ...rest,
  };
};

const register = async user => authentication(`${baseUrl}/register`, user);

const signIn = async user => authentication(`${baseUrl}/sign_in`, user);

const signOut = async user => storage.remove('token', user.token);

export {
  register,
  signIn,
  signOut,
};
