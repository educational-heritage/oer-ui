import 'babel-polyfill';
import 'material-design-lite';
import 'stylesheets/styles';

import transformers from 'utils/transformers';
import login from 'controllers/login';
import favourites from 'controllers/favourites';
import resources from 'controllers/resources';

const { transformDate } = transformers;
transformDate('.date-ago');

// Configure login window
const showLogin = login('#loginButton', '#loginDialog');

// Configure favourites
favourites('.bookmark', showLogin);

// Configure resources form
resources('#new-resource', showLogin);
