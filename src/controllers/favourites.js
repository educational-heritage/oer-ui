import { CLICK } from 'utils/events';
import {
  addResourceToFavourites,
  removeResourceFromFavourites,
} from 'services/favourites';

const addToFavourites = async resourceId => addResourceToFavourites({ id: resourceId });

const removeFromFavourites = async resourceId => removeResourceFromFavourites({ id: resourceId });

const handleClick = unauthorizedAction => async (event) => {
  let node = event.target;
  let resourceId = node.dataset.resource;
  while (!resourceId && node !== document.body) {
    node = node.parentNode;
    resourceId = node.dataset.resource;
  }
  if (resourceId) {
    const className = 'highlight';
    if (node.classList.contains(className)) {
      const { success } = await removeFromFavourites(resourceId);
      if (!success) {
        return unauthorizedAction();
      }
      node.classList.remove(className);
    } else {
      const { success } = await addToFavourites(resourceId);
      if (!success) {
        return unauthorizedAction();
      }
      node.classList.add(className);
    }
  }
  return true;
};

export default (favouriteSelector, unauthorizedAction) => {
  // Favourite buttons
  const favouriteButtons = document.querySelectorAll(favouriteSelector);

  // Event handlers
  favouriteButtons.forEach(button => button.addEventListener(
    CLICK,
    handleClick(unauthorizedAction),
  ));
};
