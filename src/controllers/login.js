import dialog from 'utils/dialog';
import { register, signIn, signOut } from 'services/authentication';
import { CLICK } from 'utils/events';
import validate from 'utils/validators';

const loginOperation = (form, operation) => async (event) => {
  event.preventDefault();
  const validated = validate(form);
  if (!validated) {
    return false;
  }

  const { success, message, reason } = await operation(new FormData(form));
  if (!success) {
    const formError = form.querySelector('.form-error');
    formError.innerHTML = `${message} ${reason}`;
    return false;
  }
  location.reload(true);
  return true;
};

const loginToggle = (content, removeClass, addClass) => () => {
  content.classList.remove(removeClass);
  content.classList.add(addClass);
};

const resetForm = (form) => {
  // Reset values and errors
  const formError = form.querySelector('.form-error');
  formError.innerHTML = '';
  form.reset();
  form
    .querySelectorAll('.input-error')
    .forEach((el) => { el.innerHTML = ''; }); // eslint-disable-line
};

export default (buttonSelector, dialogSelector) => {
  // Login button and dialog
  const loginButton = document.querySelector(buttonSelector);
  const loginDialog = document.querySelector(dialogSelector);

  const logoutButton = document.querySelector('#logoutButton');
  // Event listener for logout button
  if (logoutButton) {
    logoutButton.addEventListener(CLICK, signOut);
  }

  // Login window elements
  const signinButton = loginDialog.querySelector('#signinButton');
  const signupButton = loginDialog.querySelector('#signupButton');
  const loginContent = loginDialog.querySelector('.login-content');
  const signinForm = loginContent.querySelector('#signinForm');
  const signupForm = loginContent.querySelector('#signupForm');
  const signinSubmit = signinForm.querySelector('button');
  const signupSubmit = signupForm.querySelector('button');

  // Configure login toggle
  const loginToggleToSignIn = loginToggle(loginContent, 'login-signup', 'login-signin');
  const loginToggleToSignUp = loginToggle(loginContent, 'login-signin', 'login-signup');

  // Configure submit operations
  const signinSubmitOperation = loginOperation(signinForm, signIn);
  const signupSubmitOperation = loginOperation(signupForm, register);

  // Dialog configuration
  const showLogin = dialog(loginDialog, () => {
    // On show dialog

    // Toggle forms
    signinButton.addEventListener(CLICK, loginToggleToSignIn);
    signupButton.addEventListener(CLICK, loginToggleToSignUp);

    // Submit forms
    signinSubmit.addEventListener(CLICK, signinSubmitOperation);
    signupSubmit.addEventListener(CLICK, signupSubmitOperation);

    // Show sign in
    loginToggleToSignIn();
  }, () => {
    // On close dialog

    // Toggle forms
    signinButton.removeEventListener(CLICK, loginToggleToSignIn);
    signupButton.removeEventListener(CLICK, loginToggleToSignUp);

    // Submit forms
    signinSubmit.removeEventListener(CLICK, signinSubmitOperation);
    signupSubmit.removeEventListener(CLICK, signupSubmitOperation);

    // Reset forms
    resetForm(signinForm);
    resetForm(signupForm);
  });

  // Event listener for login button
  if (loginButton) {
    loginButton.addEventListener(CLICK, showLogin);

    // Redirection to login
    const needsLogin = loginButton.hasAttribute('data-needs-login');
    if (needsLogin) {
      setTimeout(showLogin, 1000);
    }
  }

  // Returns show login function
  return showLogin;
};
