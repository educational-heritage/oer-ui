import resources from 'services/resources';
import loadFile from 'utils/loadFile';
import {
  CHANGE,
  CLICK,
  DROP,
  DRAG_ENTER,
  DRAG_OVER,
  SUBMIT,
} from 'utils/events';
import validate from 'utils/validators';

function stopPropagationAndPreventDefault(event) {
  event.stopPropagation();
  event.preventDefault();
}

export default async (newResourceSelector, unauthorizedAction) => {
  const newResourceForm = document.querySelector(newResourceSelector);
  const preview = document.querySelector('#preview');
  const dropbox = document.querySelector('#dropbox');
  const fileSelect = document.querySelector('#fileSelect');

  if (newResourceForm) {
    // Handle image change
    newResourceForm.media.addEventListener(CHANGE, async (event) => {
      preview.src = await loadFile(event.target.files[0]);
    });

    fileSelect.addEventListener(CLICK, (event) => {
      newResourceForm.media.click();
      event.preventDefault();
    });

    // Handle drag & drop
    dropbox.addEventListener(DRAG_ENTER, stopPropagationAndPreventDefault, false);
    dropbox.addEventListener(DRAG_OVER, stopPropagationAndPreventDefault, false);
    dropbox.addEventListener(DROP, async (event) => {
      stopPropagationAndPreventDefault(event);
      preview.src = await loadFile(event.dataTransfer.files[0]);
    }, false);

    // Handle resource submit
    newResourceForm.addEventListener(SUBMIT, async (event) => {
      event.preventDefault();
      const validated = validate(newResourceForm);
      if (!validated) {
        return false;
      }

      const { success } = await resources.addResource(new FormData(newResourceForm));
      if (!success) {
        return unauthorizedAction();
      }
      location.reload(true);
      return true;
    });
  }
};
