// import config from 'config';
import storage from 'providers/storage';

const baseUrl = process.env.API_URL;

const prepareRequestHeaders = () => {
  const token = storage.get('token');
  const headers = new Headers();
  if (token) {
    headers.append('Authorization', `Bearer ${token}`);
  }
  return headers;
};

const fetchBase = async (url, options) => {
  try {
    const response = await fetch(`${baseUrl}${url}`, {
      ...options,
      headers: prepareRequestHeaders(),
    });
    const contentType = response.headers.get('Content-Type');
    if (!contentType || !contentType.includes('application/json')) {
      return {
        message: 'Error: Network response was not a JSON.',
      };
    }
    return await response.json();
  } catch (error) {
    return error;
  }
};

const GET = async url => fetchBase(url, {
  method: 'GET',
});

const POST = async (url, data) => fetchBase(url, {
  method: 'POST',
  body: data,
});

const PUT = async (url, data) => fetchBase(url, {
  method: 'PUT',
  body: data,
});

const DELETE = async url => fetchBase(url, {
  method: 'DELETE',
});

export {
  GET,
  POST,
  PUT,
  DELETE,
};
