import Cookies from 'js-cookie';

export default {
  get: key => (key ? Cookies.get(key) : Cookies.get()),
  set: (key, value) => Cookies.set(key, value, { expires: 365 }),
  remove: key => Cookies.remove(key),
};
