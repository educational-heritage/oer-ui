import moment from 'moment';

moment.locale('es', {
  months: 'enero_febrero_marzo_abril_mayo_junio_julio_agostp_septiembre_octubre_noviembre_diciembre'.split('_'),
  monthsShort: 'ene_feb_mar_abr._may_jun_jul._ago_sep._oct._nov._dec.'.split('_'),
  monthsParseExact: true,
  weekdays: 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
  weekdaysShort: 'dom_lun_mar_mie_jue_vie_sab'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_'),
  weekdaysParseExact: true,
  longDateFormat: {
    LT: 'HH:mm',
    LTS: 'HH:mm:ss',
    L: 'DD/MM/YYYY',
    LL: 'D MMMM YYYY',
    LLL: 'D MMMM YYYY HH:mm',
    LLLL: 'dddd D MMMM YYYY HH:mm',
  },
  calendar: {
    sameDay: '[Hoy] LT',
    nextDay: '[Mañana] LT',
    nextWeek: 'dddd LT',
    lastDay: '[Ayer] LT',
    lastWeek: 'dddd [última] LT',
    sameElse: 'L',
  },
  relativeTime: {
    future: 'en %s',
    past: 'hace %s',
    s: 'unos segundos',
    m: 'un minuto',
    mm: '%d minutos',
    h: 'una hora',
    hh: '%d horas',
    d: 'un día',
    dd: '%d días',
    M: 'un mes',
    MM: '%d mes',
    y: 'un año',
    yy: '%d años',
  },
  dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
  ordinal: number => (
    number + (number === 1 ? 'er' : 'e')
  ),
  meridiemParse: /PD|MD/,
  isPM: input => input.charAt(0) === 'M',
  meridiem: hours => (
    hours < 12 ? 'PD' : 'MD'
  ),
  week: {
    dow: 1, // Monday is the first day of the week.
    doy: 4, // The week that contains Jan 4th is the first week of the year.
  },
});

moment.locale('es');

export default moment;
