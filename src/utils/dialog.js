import dialogPolyfill from 'dialog-polyfill';
import {
  CLICK,
  ANIMATION_END,
} from 'utils/events';

export default (dialog, onShow, onClose) => {
  const animationEnd = (event) => {
    dialog.removeEventListener(ANIMATION_END, animationEnd);
    dialog.classList.remove(event.animationName);
    if (event.animationName === 'hide') {
      dialog.close();
      onClose();
    }
  };

  const hideDialog = (event) => {
    event.preventDefault();
    const close = event.target.classList.contains('dialog-close');
    if (close) {
      dialog.removeEventListener(CLICK, hideDialog);
      dialog.addEventListener(ANIMATION_END, animationEnd);
      dialog.classList.add('hide');
    }
  };

  // Polyfill dialog 
  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
  }

  // Return launch function
  return (event) => {
    if (event) {
      event.preventDefault();
    }
    dialog.addEventListener(CLICK, hideDialog);
    dialog.addEventListener(ANIMATION_END, animationEnd);
    dialog.classList.add('show');
    dialog.showModal();
    onShow();
  };
};
