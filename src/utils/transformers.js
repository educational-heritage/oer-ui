import moment from 'utils/moment';

const transformDate = (selector, baseElement = document) => {
  const els = baseElement.querySelectorAll(selector);
  els.forEach(el => Object.assign(el, {
    innerHTML: moment(new Date(el.innerHTML)).fromNow(),
  }));
};

export default {
  transformDate,
};
