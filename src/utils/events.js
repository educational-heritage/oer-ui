export const ANIMATION_END = 'webkitAnimationEnd';
export const CHANGE = 'change';
export const CLICK = 'click';
export const DROP = 'drop';
export const DRAG_ENTER = 'dragenter';
export const DRAG_OVER = 'dragover';
export const SUBMIT = 'submit';
