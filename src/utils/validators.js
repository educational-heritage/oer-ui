const rules = {
  email: {
    test: value => /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(value),
    message: 'Este campo debe tener un formato de correo válido',
  },
  username: {
    test: value => /^[a-zA-Z0-9]{3,10}$/.test(value),
    message: 'Este campo debe tener entre 3 y 10 caracteres',
  },
  password: {
    test: value => /^[a-zA-Z0-9]{6,10}$/.test(value),
    message: 'Este campo debe tener enter 6 y 10 caracteres',
  },
  fullname: {
    test: value => /^[A-Za-z]+\s+[A-Za-z]+$/.test(value),
    message: 'Este campo debe tener nombre y apellido',
  },
  title: {
    test: value => /.*\S.*/.test(value),
    message: 'Este campo debe tener contenido',
  },
  media: {
    test: value => /.*\S.*/.test(value),
    message: 'Este campo debe tener contenido',
  },
  content: {
    test: value => /.*\S.*/.test(value),
    message: 'Este campo debe tener contenido',
  },
};

export default (form) => {
  const errors = {};

  // Validate fields
  const validateField = ({ name, value }) => {
    const validator = rules[name];
    errors[name] = validator && !validator.test(value)
      ? validator.message
      : '';
  };

  // Show validations
  const showFieldValidation = ({ nextSibling: inputError, name }) => {
    inputError.innerHTML = errors[name] || ''; // eslint-disable-line
  };

  // Validate all fields
  form.querySelectorAll('input,textarea').forEach((input) => {
    validateField(input);
    showFieldValidation(input);
  });

  // Return errors
  return Object.values(errors).every(error => !error);
};
